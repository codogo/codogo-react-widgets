// you can use this file to add your custom webpack plugins, loaders and anything you like.
// This is just the basic way to add addional webpack configurations.
// For more information refer the docs: https://goo.gl/qPbSyX

// IMPORTANT
// When you add this file, we won't add the default configurations which is similar
// to "React Create App". This only has babel loader to load JavaScript.

module.exports = {
	devtool: 'cheap-module-eval-source-map',

	plugins: [
		// your custom plugins
	],
	module: {
		loaders: [
			{
				test: /\.svg$/,
				loader: "svg-url-loader",
			},
			{
				test: /\.json/,
				loader: "json-loader",
			},
			{
				test: /\.scss?$/,
				loaders: [ "style", "css", "sass", ],
			},
		],
	},

	node: {
		fs: "empty",
	},

	resolve: {
		extensions: [ "", ".js", ".jsx", ],
	},
};
