# Codogo React Widgets

Provides a unified way to access the styling of commonly used widgets across different apps

## API

### js

```
import { CodogoTick, CodogoRadio, CodogoButton } from "codogo-react-widgets";
require("codogo-react-widgets/sass/main.scss");
```

# SCSS

```
@import "/path/to/node_modules/codogo-react-widgets/scss/main.scss
$codogo-pallet-main: //the main colour of the app (eg: orange for write)
$codogo-pallet-background: //the off-white background of the app
$codogo-pallet-slight: //the colour for drawing slight attention (think hover)
$codogo-pallet-highlight: //colour for drawing more attention (think select)
$codogo-pallet-warn: //colour for drawing attention  to a potentialy important choice (think "are you sure you want to continue")
$codogo-pallet-danger: //colour for drawing attention  to  a  potentioaly dangerous choice (think "are you sure you want to delete this document")
```
