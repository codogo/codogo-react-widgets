SRC = $(shell find src -name "*.js")
DIST = $(SRC:src/%.js=dist/%.js)

IMG_SRC = $(shell find img -name "*.svg")
IMG_DIST = $(IMG_SRC:src/%.svg=dist/%.svg)

all: clear $(DIST) $(DIST_IMG)
clear:
	@ echo "--------"
	@ echo "Last Run"
	@ date +"%H:%M:%S"
	@ echo "--------"

dist/%.js: src/%.js makefile package.json .babelrc
	@ mkdir -p $(@D)
	@ BABEL_ENV="production" babel $< -o $@ --source-maps
	@ echo $@

dist/%.svg: src/%.svg makefile package.json
	@ mkdir -p $(@D)
	@ cp $< $@
	@ echo $@

clean:
	rm -rf dist/*

