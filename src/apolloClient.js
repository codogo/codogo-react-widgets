import ApolloClient from "apollo-client";
import { HttpLink, } from "apollo-link-http";
import { split, } from "apollo-link";
import { InMemoryCache, } from "apollo-cache-inmemory";
import { WebSocketLink, } from "apollo-link-ws";
import { getMainDefinition, } from "apollo-utilities";
import { setContext, } from "apollo-link-context";

import getJWT from "./getJWT";
import { graphcoolURL, graphcoolSubURL, } from "./consts";

//------------------------------
//http

const rawHTTPLink = new HttpLink({ uri: graphcoolURL, });

const authMiddleware = setContext((operation, oldContext) =>
	getJWT().then(jwt =>
		R.assocPath(
			[ "headers", "authorization", ],
			jwt ? `Bearer ${ jwt }` : undefined,
			oldContext,
		),
	),
);

const httpLink = authMiddleware.concat(rawHTTPLink);

//------------------------------
//subscription

//the JWT is fetched asyncnsly, but the WebSocketLink needs it sync
//we define a varible here that will be populated with the JWT as fast as possible.
//then `connectionParams` below uses a getter to provide the most up to date version
//of `asyncAuthToken`.
let asyncAuthToken = undefined;
getJWT().then(jwt => {
	asyncAuthToken = jwt;
});

const connectionParams = {
	get Authorization() {
		return `Bearer ${ asyncAuthToken }`;
	},
};

const wsLink = new WebSocketLink({
	uri: graphcoolSubURL,
	options: {
		reconnect: true,
		timeout: 20000,
		connectionParams,
	},
});

//------------------------------
//join

const link = split(
	// split based on operation type
	({ query, }) => {
		const { kind, operation, } = getMainDefinition(query);
		return kind === "OperationDefinition" && operation === "subscription";
	},
	wsLink,
	httpLink,
);

//------------------------------
//cache

const cache = new InMemoryCache({});

//------------------------------
//client

export const client = new ApolloClient({
	cache,
	link,
});
