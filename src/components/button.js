import React from "react";
import PropTypes from "prop-types";
import styled, { css, } from "styled-components";
import { Link, } from "react-router-dom";

import {
	getPaddingHorizontal,
	getPaddingVertical,
	getMarginFourEm,
} from "../getters";

import Icon from "./icon";

// --------------------------------------------------

const getButtonMaxHeight = R.pipe(
	getPaddingVertical,
	R.multiply(2),
	R.add(1.2),
);

// --------------------------------------------------

const getButtonTextColor = ({
	theme: { colors = {}, },
	type,
	textColorOverride,
}) =>
	textColorOverride ||
	{
		CONFIRM: colors.white,
		SELECT: colors.white,
		CANCEL: colors.white,
		WHITE: colors.primary,
		PLAIN: colors.darkgray,
	}[type];

const getButtonBorderColor = ({
	theme: { colors = {}, },
	type,
	colorOverride,
}) =>
	colorOverride ||
	{
		CONFIRM: colors.primary,
		SELECT: colors.secondary,
		CANCEL: colors.darkgray,
		WHITE: colors.primary,
		PLAIN: colors.darkgray,
	}[type];

const getButtonBackgroundColor = ({
	theme: { colors = {}, },
	type,
	colorOverride,
}) =>
	colorOverride ||
	{
		CONFIRM: colors.primary,
		SELECT: colors.secondary,
		CANCEL: colors.darkgray,
		WHITE: colors.white,
		PLAIN: colors.white,
	}[type];

const buttonStyle = css`
	background-color: ${ getButtonBackgroundColor };
	border-radius: ${ R.path([ "theme", "sizes", "borderRadius", ]) };
	border: 1px solid ${ getButtonBorderColor };
	color: ${ getButtonTextColor } !important;
	cursor: pointer;
	margin: ${ getMarginFourEm };
	max-height: ${ getButtonMaxHeight }em;
	padding: ${ getPaddingVertical }em ${ getPaddingHorizontal }em;
	position: relative;
	transition: 0.1s all linear;
	vertical-align: bottom;
	display: inline-flex;
	align-items: center;
	flex-direction: row;

	${ props =>
		props.shadow && R.path([ "theme", "functions", "shadowProxy", 1, ]) };

	&:hover,
	&:active {
		opacity: ${ R.path([ "theme", "colors", "translucent", ]) };
		${ props =>
		props.shadow &&
			`
			${ R.path([ "theme", "functions", "shadowProxy", 2, ]) }
			transform: translateY(-2px);
		` };
	}

	span {
		align-self: flex-end;
	}
`;

const ButtonDiv = styled.div`
	${ buttonStyle };
`;

const ButtonLink = styled(Link)`
	${ buttonStyle };
`;

const ButtonA = styled.a`
	${ buttonStyle };
`;

const ButtonIcon = styled(Icon)`
	height: 1.33em;
	width: 1.33em;
	margin-right: 0.33em;
`;

// --------------------------------------------------

const passedProps = [
	"type",
	"onClick",
	"to",
	"href",
	"target",
	"rel",
	"colorOverride",
	"textColorOverride",
	"shadow",
];

const Button = props => {
	let ButtonStyled = ButtonDiv;
	if (props.href) {
		ButtonStyled = ButtonA;
	}
	if (props.to) {
		ButtonStyled = ButtonLink;
	}
	return (
		<ButtonStyled { ...R.pick(passedProps)(props) }>
			{props.iconKey && <ButtonIcon iconKey = { props.iconKey } />}

			{props.text || props.children}
		</ButtonStyled>
	);
};

Button.propTypes = {
	children: PropTypes.oneOfType([ PropTypes.string, PropTypes.element, ]),
	onClick: PropTypes.func,
	type: PropTypes.oneOf([ "SELECT", "CONFIRM", "CANCEL", "WHITE", "PLAIN", ])
		.isRequired,
	fullWidth: PropTypes.bool,
	text: PropTypes.string,
	active: PropTypes.bool,
};

Button.defaultProps = {
	type: "CONFIRM",
	shadow: true,
};

// --------------------------------------------------

const buttonInContainerStyle = css`
	margin: ${ R.pipe(
		R.path([ "theme", "sizes", "button", "gutter", ]),
		x => 0.5 * x,
	) }em;
`;

const ButtonsContainer = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: ${ props => (props.alignRight ? "flex-end" : "flex-start") };
	flex-wrap: wrap;
	margin: -${ R.pipe(R.path([ "theme", "sizes", "button", "gutter", ]), x => 0.5 * x) }em;

	& > ${ ButtonDiv } {
		${ buttonInContainerStyle };
	}
	& > ${ ButtonLink } {
		${ buttonInContainerStyle };
	}
	& > ${ ButtonA } {
		${ buttonInContainerStyle };
	}
`;

Button.Container = ButtonsContainer;

// --------------------------------------------------

export default Button;
