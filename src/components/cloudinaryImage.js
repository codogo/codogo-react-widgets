import React from "react";
import styled from "styled-components";
import cloudinary from "cloudinary-core";
import * as R from "ramda";
import { compose, withHandlers, } from "recompose";

//------------------------------

const PRESET_NAME = "UnAuthed_LimitSize";
const CLOUDINARY_URL = "https://api.cloudinary.com/v1_1/codogo/upload";
const DEFAULT_IMAGE = "Codogo-Logo-C_rgkd8u";
const cl = cloudinary.Cloudinary.new({ cloud_name: "codogo", });

const Preloader = styled.div`
	background-color: ${ ({ theme, }) => theme.colors.lightgray };
	${ props => {
		if (props.width && props.height) {
			return `
				width: ${props.width }px;
				height: ${props.height }px;
			`;
		} else if (props.aspectRatio) {
			return `
				width: 100%;
				padding-top: ${100 / props.aspectRatio }%;
			`;
		} else {
			return `
				width: 100%;
				padding-top: 75%;
			`;
		}
	} };
	`;

const enhance = compose(
	withHandlers({
		renderPreloader: props => () => <Preloader { ...props } />,
	}),
);

const CloudinaryImage = enhance(
	({
		cloudinaryID,
		cloudinaryId,
		height,
		transformation,
		transformationPreScale,
		transformations,
		transformationsPreScale,
		url,
		uri,
		width,
	}) => {
		const allTransformations = [
			...(transformationsPreScale || []),
			transformationPreScale,
			{
				crop: "fill",
				dpr: window.devicePixelRatio,
				fetch_format: "auto",
				height: height,
				quality: "auto",
				width: width,
			},
			...(transformations || []),
			transformation,
		].filter(R.identity);

		const src = cl.url(
			uri || url || cloudinaryID || cloudinaryId || DEFAULT_IMAGE,
			{
				secure: true,
				type: url || uri ? "fetch" : undefined,
				transformation: allTransformations,
			},
		);

		return (
			<img
				alt = "TODO: introduce a new loading component"
				src = { src }
				style = { {
					width: width || "100%",
					height: height || "auto",
				} }
				className
			/>
		);
	},
);

CloudinaryImage.commonTransforms = {
	smartSquare: {
		aspect_ratio: 1,
		gravity: "auto",
		crop: "fill",
	},
};

CloudinaryImage.getUrl = ({ uri, url, cloudinaryId, cloudinaryID, id, }) =>
	cl.url(uri || url || cloudinaryId || cloudinaryID || id || DEFAULT_IMAGE, {
		secure: true,
		type: url || uri ? "fetch" : undefined,
	});

export default CloudinaryImage;
