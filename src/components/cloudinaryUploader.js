import React from "react";
import styled from "styled-components";
import * as R from "ramda";
import Dropzone from "react-dropzone";
import request from "superagent";
import { compose, withHandlers, } from "recompose";
import Button from "./button";

// --------------------------------------------------

const PRESET_NAME = "UnAuthed_LimitSize";
const CLOUDINARY_URL = "https://api.cloudinary.com/v1_1/codogo/upload";

const uploadImageToCloudinary = file => {
	const upload = request
		.post(CLOUDINARY_URL)
		.field("upload_preset", PRESET_NAME)
		.field("file", file);

	return new Promise((good, fail) =>
		upload.end((err, resp) => {
			if (!err && resp && resp.body && resp.body.public_id) {
				good(resp.body);
			} else {
				fail(err || resp);
			}
		}),
	);
};

const enhance = compose(
	withHandlers({
		onDrop: props => ([ file, ]) => {
			if (props.onDropFile) {
				props.onDropFile(file);
			}

			uploadImageToCloudinary(file)
				.then(
					R.pipe(
						({ public_id, secure_url, }) => ({
							cloudinaryID: public_id,
							uri: secure_url,
						}),
						props.onUpload || R.identity,
					),
				)
				.then(() => window.URL.revokeObjectURL(file.preview))
				.catch(props.onFail || R.identity);
		},
	}),
);

const DropzoneWrapper = styled.div`
	display: flex;
	width: 100%:
	height: 100%;
	min-height: 200px;
	border-radius: 0.5em;
	border: 2px dashed ${ props =>
		props.active ? props.theme.colors.blue : props.theme.colors.lightGrey };
	align-items: center;
	justify-content: center;
	text-align: center;
	transition: 0.2s border-color linear;
`;

const DropzoneText = styled.div`
	font-size: 1em;
	width: 50%;
`;

const DropzoneImagePreview = styled.div`
	width: 20%;
	padding-top: 20%;
	background-image: url(${ R.prop("src") });
	background-size: cover;
	background-position: center;
`;

// props: { isDragActive, isDragReject, acceptedFiles, rejectedFiles }
const DropzoneContent = props => {
	if (props.acceptedFiles.length) {
		return (
			<DropzoneWrapper active>
				<DropzoneImagePreview src = { props.acceptedFiles[0].preview } />
				<DropzoneText>Uploading...</DropzoneText>
			</DropzoneWrapper>
		);
	} else if (props.isDragActive) {
		return (
			<DropzoneWrapper active>
				<DropzoneText>drop your file here!</DropzoneText>
			</DropzoneWrapper>
		);
	} else {
		return (
			<DropzoneWrapper>
				<DropzoneText>drag files here or</DropzoneText>
				<Button type = "CONFIRM">Choose a file</Button>
			</DropzoneWrapper>
		);
	}
};

const CloudinaryUploader = props => (
	<Dropzone
		accept = "image/*"
		onDrop = { props.onDrop }
		style = { {
			width: "100%",
			height: "100%",
			minHeight: "200px",
		} }
	>
		{DropzoneContent}
	</Dropzone>
);

export default enhance(CloudinaryUploader);
