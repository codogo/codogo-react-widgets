import React from "react";
import * as R from "ramda";
import styled from "styled-components";
import autobind from "autobind-decorator";

import {
	getArrowWidth,
	getBorderRadius,
	getColor,
	getPadding2,
	getPaddingHorizontal,
	getPaddingVertical,
} from "../getters";

// --------------------------------------------------

const getHalfArrowWidth = R.pipe(getArrowWidth, R.multiply(0.5));

// --------------------------------------------------

const Wrapper = styled.div``;

const Trigger = styled.div`
	position: relative;
	cursor: pointer;
	padding-left: ${ getPaddingHorizontal }em;
	margin-bottom: ${ getPaddingVertical }em;

	&:hover {
		opacity: ${ R.path([ "theme", "colors", "translucent", ]) };
	}

	&:before {
		margin-top: 3px;
		content: "";
		position: absolute;
		left: 0;
		border: ${ getHalfArrowWidth }em solid transparent;
		border-top-width: ${ getArrowWidth }em;
		border-bottom-width: 0;
		border-top-color: ${ getColor("darkgray") };
		height: ${ getArrowWidth }em;
		width: ${ getArrowWidth }em;
		transform: rotate(${ props => (props.active ? "0deg" : "-90deg") });
	}
`;

const Content = styled.div`
	background: ${ getColor("lightgray") };
	border-radius: ${ getBorderRadius };
	border: 1px solid ${ getColor("gray") };
	display: ${ props => (props.active ? "block" : "none") };
	padding: ${ getPadding2 };

	&.scrolled {
		box-shadow: none;
	}
`;

// --------------------------------------------------

class Collapsable extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			active: false,
		};
	}

	@autobind
	toggle() {
		this.setState(pState => ({ active: !pState.active, }));
	}

	render() {
		return (
			<Wrapper>
				<Trigger onClick = { this.toggle } { ...this.state }>
					{this.props.label ? this.props.label : "Collapsed Content"}
				</Trigger>

				<Content { ...this.state }>{this.props.children}</Content>
			</Wrapper>
		);
	}
}

// --------------------------------------------------

export default Collapsable;
