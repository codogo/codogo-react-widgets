import React from "react";
import styled from "styled-components";
import autobind from "autobind-decorator";
import clickOutside from "react-onclickoutside";
import * as R from "ramda";
import Icon from "./icon";

// --------------------------------------------------

const Menu = styled.div`
	background: white;
	width: 20em;
	padding: 0.5em 0;
	position: fixed;
	${ props => {
		const s = R.path([ "theme", "functions", "shadow", ])(props);
		if (s) {
			return s(2);
		} else {
			return `
				box-shadow:
					0px	3px 3px -3px rgba(0, 0, 0, 0.3),
					0px 3px 6px 3px rgba(0, 0, 0, 0.15);
			`;
		}
	} };
	z-index: 1000000000;
`;

const Option = styled.div`
	padding: 0.5em 1em;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	cursor: pointer;
	display: flex;
	flex-direction: row;

	&:hover {
		background: rgba(0, 0, 0, 0.1);
	}
`;

const Label = styled.span`
	line-height: 1;
`;

const CMIcon = styled(Icon)`
	margin-right: 0.5em;
`;

// --------------------------------------------------

const optionCallBackAndClose = close => fn => e => {
	e.preventDefault();
	e.stopPropagation();
	if (fn) {
		fn(e);
	}
	close();
};

@clickOutside
class ContextMenu extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			top: true,
			left: true,
		};
	}

	@autobind
	handleClickOutside() {
		this.props.close();
	}

	@autobind
	menuRef(el) {
		if (el) {
			const bounds = el.getBoundingClientRect();
			if (bounds.bottom > window.innerHeight) {
				this.setState({ top: false, });
			}
			if (bounds.right > window.innerWidth) {
				this.setState({ left: false, });
			}
		}
	}

	render() {
		const props = this.props;
		const style = {};
		style.top = this.state.top ? props.y : null;
		style.bottom = this.state.top ? null : window.innerHeight - props.y;
		style.left = this.state.left ? props.x : null;
		style.right = this.state.left ? null : window.innerWidth - props.x;

		const optionCallback = optionCallBackAndClose(props.close);

		// const optionsWithIcons = R.pipe(
		// 	R.when(
		// 		R.any(R.prop("icon")),
		// 		R.map(R.over(R.lensProp("icon"), R.defaultTo(ChevronIcon))),
		// 	),
		// )(this.props.options);

		return (
			<Menu style = { style } innerRef = { this.menuRef }>
				{this.props.options.map((option, i) => (
					<Option key = { i } onClick = { optionCallback(option.fn) }>
						{option.icon ? (
							<CMIcon icon = { option.icon } />
						) : (
							<CMIcon
								iconKey = { option.iconKey || "chevron-right" }
							/>
						)}
						<Label>{option.label}</Label>
					</Option>
				))}
			</Menu>
		);
	}
}

export default ContextMenu;
