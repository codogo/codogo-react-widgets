import React from "react";
import * as R from "ramda";
import styled from "styled-components";
import autobind from "autobind-decorator";
import clickOutside from "react-onclickoutside";
import ReactDOM from "react-dom";

import { lighten, } from "polished";

import {
	getArrowWidth,
	getBorderRadius,
	getColor,
	getMarginLeft,
	getMarginRight,
	getMarginVertical,
	getPaddingHorizontal,
	getPaddingVertical,
	getPadding2,
} from "../getters";

// --------------------------------------------------

const getWrapperMaxHeight = R.pipe(getPaddingVertical, R.multiply(2), R.add(1));

const getWrapperMaxWidth = R.pipe(
	getPaddingHorizontal,
	R.multiply(2),
	R.add(1),
);

// --------------------------------------------------

const Wrapper = styled.div`
	position: relative;
	line-height: 1;
	// max-height: ${ getWrapperMaxHeight }em;
	display: inline-block;
	overflow: visible;
	cursor: pointer;
	width: 10em;
	flex: 7;
`;

const getTriggerPadding = props =>
	[
		getPaddingVertical(props),
		getWrapperMaxWidth(props),
		getPaddingVertical(props),
		getPaddingHorizontal(props),
	].join("em ") + "em";

const Trigger = styled.div`
	background: ${ getColor("lightgray") };
	border-radius: ${ getBorderRadius };
	border: 1px solid ${ getColor("gray") };
	padding: ${ getTriggerPadding };
	position: relative;

	&:hover {
		background: ${ getColor("midgray") };
	}

	&:before {
		content: "";
		position: absolute;
		top: ${ props =>
		getPaddingVertical(props) + (1 - getArrowWidth(props)) / 2 }em;
		right: ${ getPaddingHorizontal }em;
		border: ${ props => getArrowWidth(props) / 2 }em solid transparent;
		border-top-width: ${ getArrowWidth }em;
		border-bottom-width: 0;
		border-top-color: ${ getColor("darkgray") };
		height: ${ getArrowWidth }em;
		width: ${ getArrowWidth }em;
	}
`;

const getContentBorderRadius = props =>
	[ "0", "0", getBorderRadius(props), getBorderRadius(props), ].join(" ");

const Content = styled.div`
	background: ${ getColor("lightgray") };
	border-radius: ${ getContentBorderRadius };
	border: 1px solid ${ getColor("gray") };
	box-shadow: inset 0 -2em 2em -2em ${ R.path([ "theme", "colors", "darkgray", ]) };
	display: ${ p => (p.active ? "block" : "none") };
	left: 0;
	max-height: 60vh;
	overflow-y: scroll;
	padding: 0;
	position: absolute;
	right: 0;
	top: 100%;
	z-index: 10;

	${ ({ scrolled, }) =>
		scrolled
			? `
		box-shadow: none;
	`
			: "" };
`;

const getOptionBackgroundColor = props =>
	props.active
		? `background-color: ${ lighten(0.4, getColor("primary")(props)) };`
		: "";

const Option = styled.div`
	${ getOptionBackgroundColor } overflow: hidden;
	padding: ${ getPaddingVertical }em ${ getPaddingHorizontal }em;
	text-overflow: ellipsis;
	white-space: nowrap;

	&:hover {
		background: ${ getColor("midgray") };
	}
`;

const getOuterWrapperMargin = props =>
	[
		getMarginVertical(props),
		getMarginRight(props),
		getMarginVertical(props),
		getMarginLeft(props),
	].join("em ") + "em";

const OuterWrapper = styled.div`
	align-items: center;
	display: flex;
	flex-direction: row;
	margin: ${ getOuterWrapperMargin };
`;

const Label = styled.div`
	padding: ${ getPadding2 };
	flex: 3;
	text-align: right;
	border: 1px transparent;
`;

// --------------------------------------------------

class DropdownOption extends React.Component {
	@autobind
	onClick() {
		this.props.onClick({
			value: this.props.value || this.props.children,
			target: {
				value: this.props.value || this.props.children,
			},
		});
	}

	render() {
		return <Option { ...this.props } onClick = { this.onClick } />;
	}
}

@clickOutside
class Dropdown extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			active: false,
			scrolled: false,
		};
	}

	@autobind
	calculateScrollState() {
		if (
			this.dropdownContentRef.scrollHeight -
				this.dropdownContentRef.clientHeight -
				this.dropdownContentRef.scrollTop ===
			0
		) {
			this.setState({ scrolled: true, });
		}
	}

	@autobind
	toggle() {
		this.setState(
			pState => ({ active: !pState.active, }),
			this.calculateScrollState,
		);
	}

	@autobind
	show() {
		this.setState({ active: true, });
	}

	@autobind
	hide() {
		this.setState({ active: false, });
	}

	@autobind
	onClickOption(e) {
		this.hide();
		this.props.onChange(e);
	}

	@autobind
	handleClickOutside() {
		this.hide();
	}

	//Scrolling

	@autobind
	onWrapperScroll(e) {
		if (
			e.target.scrollHeight -
				e.target.clientHeight -
				e.target.scrollTop ===
			0
		) {
			this.setState({ scrolled: true, });
		} else {
			this.state.scrolled && this.setState({ scrolled: false, });
		}
	}

	componentDidMount() {
		const dropdownContent = ReactDOM.findDOMNode(this.dropdownContentRef);
		dropdownContent.addEventListener("scroll", this.onWrapperScroll);
	}

	componentWillUnmount() {
		const dropdownContent = ReactDOM.findDOMNode(this.dropdownContentRef);
		dropdownContent.removeEventListener("scroll", this.onWrapperScroll);
	}

	@autobind
	onDropdownContentRef(ref) {
		this.dropdownContentRef = ref || this.dropdownContentRef;
	}

	render() {
		const { label, value, children, } = this.props;

		return (
			<OuterWrapper>
				{label && <Label>{label}</Label>}

				<Wrapper>
					<Trigger onClick = { this.toggle }>
						{!!value
							? (
								children.find(
									o =>
										value === o.props.value ||
											value === o.props.children,
								) || children[0]
							).props.children
							: this.props.default || "Choose..."}
					</Trigger>

					<Content
						innerRef = { this.onDropdownContentRef }
						{ ...this.state }
					>
						{this.props.children &&
						this.props.children.length > 0 ? (
								React.Children.map(
									children,
									({ props: optionProps, }, i) => (
										<DropdownOption
											key = { i }
											{ ...optionProps }
											onClick = { this.onClickOption }
											active = { value === optionProps.value }
										/>
									),
								)
							) : (
								<Option onClick = { this.hide }>
								No options supplied
								</Option>
							)}
					</Content>
				</Wrapper>
			</OuterWrapper>
		);
	}
}

const Blank = props => <div { ...props } />;

Dropdown.Option = Blank;

Dropdown.Trigger = props => <Option { ...props } trigger />;

// --------------------------------------------------

Dropdown.Old = props => (
	<form>
		<select value = { props.value } onChange = { props.onChange }>
			{props.children}
		</select>
	</form>
);

Dropdown.Old.Option = props => (
	<option value = { props.value }>{props.children}</option>
);

// --------------------------------------------------

export default Dropdown;
