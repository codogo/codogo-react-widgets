import React from "react";
import styled from "styled-components";
import * as R from "ramda";
import Dropzone from "react-dropzone";
import request from "superagent";
import { compose, withHandlers, } from "recompose";
import Button from "./button";

// --------------------------------------------------

const enhance = compose(
	withHandlers({
		onDrop: props => ([ file, ]) => {
			if (props.onDropFile) {
				props.onDropFile(file);
			}

			if (props.uploadUrl) {
				return request
					.post(props.uploadUrl)
					.field("file", file)
					.end((err, resp) => {
						(!err && resp
							? Promise.resolve(resp)
							: Promise.reject(err || resp)
						)
							.then(props.onUpload || R.identity)
							.then(() =>
								window.URL.revokeObjectURL(file.preview),
							)
							.catch(props.onFail || R.identity);
					});
			} else {
				return Promise.reject("props.uploadUrl required");
			}
		},
	}),
);

const DropzoneWrapper = styled.div`
	display: flex;
	width: 100%:
	height: 100%;
	min-height: 200px;
	border-radius: 0.5em;
	border: 2px dashed ${ props =>
		props.active ? props.theme.colors.blue : props.theme.colors.lightGrey };
	align-items: center;
	justify-content: center;
	text-align: center;
	transition: 0.2s border-color linear;
`;

const DropzoneText = styled.div`
	font-size: 1em;
	width: 50%;
`;

const DropzoneImagePreview = styled.div`
	width: 20%;
	padding-top: 20%;
	background-image: url(${ R.prop("src") });
	background-size: cover;
	background-position: center;
`;

// props: { isDragActive, isDragReject, acceptedFiles, rejectedFiles }
const DropzoneContent = props => {
	if (props.acceptedFiles.length) {
		return (
			<DropzoneWrapper active>
				<DropzoneImagePreview src = { props.acceptedFiles[0].preview } />
				<DropzoneText>Uploading...</DropzoneText>
			</DropzoneWrapper>
		);
	} else if (props.isDragActive) {
		return (
			<DropzoneWrapper active>
				<DropzoneText>drop your file here!</DropzoneText>
			</DropzoneWrapper>
		);
	} else {
		return (
			<DropzoneWrapper>
				<DropzoneText>drag files here or</DropzoneText>
				<Button type = "CONFIRM">Choose a file</Button>
			</DropzoneWrapper>
		);
	}
};

const Uploader = props => (
	<Dropzone
		onDrop = { props.onDrop }
		style = { {
			width: "100%",
			height: "100%",
			minHeight: "200px",
		} }
	>
		{DropzoneContent}
	</Dropzone>
);

export default enhance(Uploader);
