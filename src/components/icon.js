import styled, { withTheme, } from "styled-components";
import * as R from "ramda";
import feather from "feather-icons";

const IconWrapper = styled.div`
	height: ${ R.propOr("1em", "size") };
	width: ${ R.propOr("1em", "size") };
`;

const NoIcon = styled.div`
	width: 100%;
	height: 100%;
	background-color: ${ R.prop("stroke") };
`;

const Icon = props => {
	const stroke =
		R.path([ "theme", "colors", props.stroke, ], props) ||
		props.stroke ||
		"currentColor";

	if (props.iconKey) {
		const dsih = {
			__html: feather.icons[props.iconKey].toSvg({
				width: "100%",
				height: "100%",
				stroke,
				...(props.strokeWidth
					? {
						strokeWidth: props.strokeWidth,
					}
					: {}),
			}),
		};

		return <IconWrapper { ...props } dangerouslySetInnerHTML = { dsih } />;
	}
	if (props.icon) {
		return (
			<IconWrapper { ...props }>
				<props.icon
					width = "100%"
					height = "100%"
					stroke = { stroke }
					color = { stroke }
					{ ...R.pick([ "strokeWidth", ])(props) }
				/>
			</IconWrapper>
		);
	} else {
		return (
			<IconWrapper { ...props }>
				<NoIcon stroke = { stroke } />
			</IconWrapper>
		);
	}
};

export default withTheme(Icon);
