import React from "react";
import PropTypes from "prop-types";
import styled, { css, } from "styled-components";
import TextareaAutosize from "react-autosize-textarea";

import {
	getBorderRadius,
	getColor,
	getMargin2,
	getPadding2,
	getPaddingVertical,
} from "../getters";

// --------------------------------------------------

const getInputMaxHeight = R.pipe(getPaddingVertical, R.multiply(2), R.add(1.2));

// --------------------------------------------------

const OuterWrapper = styled.div`
	display: flex;
	flex-direction: row;
	margin: ${ getMargin2 };
	${ props => (props.plain ? "margin: 0;" : "") };
`;

const Label = styled.div`
	padding: ${ getPadding2 };
	flex: 3;
	text-align: right;
	border: 1px transparent;
`;

const inputStyle = css`
	appearance: none;
	border-radius: ${ getBorderRadius };
	border: 1px solid
		${ R.ifElse(R.prop("valid"), getColor("gray"), getColor("red")) };
	box-shadow: none;
	flex: 7;
	font-family: inherit;
	font-size: 1em;
	margin: 0;
	padding: ${ getPadding2 };
	width: 100%;

	${ props =>
		props.plain
			? `
		border-radius: 0;
		border: 0;
		padding: 0;
	`
			: "" } &:focus {
		outline: none;
	}
`;

const SingleLineInput = styled.input`
	${ inputStyle } max-height: ${ getInputMaxHeight }em;
	text-overflow: ellipsis;
`;

const MultiLineInput = styled(TextareaAutosize)`
	${ inputStyle } box-sizing: content-box;
	${ ({ initialSize, }) =>
		initialSize ? `min-height: ${ initialSize }em;` : "" };
`;

// --------------------------------------------------

const Input = props => {
	const { label, multiline, ...rest } = props;

	return (
		<OuterWrapper plain = { rest.plain }>
			{label && <Label>{label}</Label>}
			{multiline ? (
				<MultiLineInput { ...rest } />
			) : (
				<SingleLineInput { ...rest } />
			)}
		</OuterWrapper>
	);
};

Input.propTypes = {
	flavor: PropTypes.oneOf([ "BLACK", "WHITE", "TRANSPARENT", ]),
	fullWidth: PropTypes.bool,
	label: PropTypes.string,
	message: PropTypes.string,
	multiline: PropTypes.bool,
	name: PropTypes.string.isRequired,
	onBlur: PropTypes.func,
	onChange: PropTypes.func,
	placeholder: PropTypes.string,
	required: PropTypes.bool,
	type: PropTypes.oneOf([ "EMAIL", "PASSWORD", "TEXT", ]),
	valid: PropTypes.bool,
	value: PropTypes.string,
};

Input.defaultProps = {
	valid: true,
};

export default Input;
