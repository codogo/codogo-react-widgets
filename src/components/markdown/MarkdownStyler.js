import styled from "styled-components";

const MarkdownStyler = styled.div`
	div {
		cursor: text;
		display: inline-block;
		margin: 0;
		outline: 0px solid transparent;
		pointer-events: all;
		tabSize: 4;
		white-space: pre-wrap;
		width: 100%;

		&:empty {
			display: block;
		}

		&:empty::before {
			content: "${ R.prop("placeholder") }";
			opacity: 0.33;
		}
	}

	a,
	a:hover,
	a:visited,
	a:active,
	a:link {
		color: ${ R.path([ "theme", "colors", "primary", ]) };
		max-width: 100%;
		cursor: pointer;
		text-decoration: underline;
		word-wrap: break-word;
	}

	.hljs-emphasis {
		color: ${ R.path([ "theme", "colors", "red", ]) };
	}
	.hljs-strong {
		color: ${ R.path([ "theme", "colors", "yellow", ]) };
	}
	.hljs-string {
		color: ${ R.path([ "theme", "colors", "green", ]) };
	}
	.hljs-link {
		color: ${ R.path([ "theme", "colors", "blue", ]) };
	}
	.hljs-code {
		color: ${ R.path([ "theme", "colors", "orange", ]) };
	}
`;

export default MarkdownStyler;
