/* eslint-disable */

export const isFirefox = typeof InstallTrigger !== "undefined";
export const isOpera =
	(!!window.opr && !!window.opr.addons) ||
	!!window.opera ||
	navigator.userAgent.indexOf(" OPR/") >= 0;
export const isSafari =
	/export constructor/i.test(window.HTMLElement) ||
	(function(p) {
		return p.toString() === "[object SafariRemoteNotification]";
	})(
		!window["safari"] ||
			(typeof safari !== "undefined" && safari.pushNotification),
	);
//export const isIE = [>@cc_on!@<] false || !!document.documentMode;
//export const isEdge = !isIE && !!window.StyleMedia;
export const isChrome = !!window.chrome && !!window.chrome.webstore;
export const isBlink = (isChrome || isOpera) && !!window.CSS;
