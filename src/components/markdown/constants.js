const BACKSPACE = 8;
//const TAB = 9;
//const ENTER = 13;
//const SHIFT = 16;
//const CTRL = 17;
//const ALT = 18;
const PAUSE_BREAK = 19;
const CAPS_LOCK = 20;
//const ESCAPE = 27;
const PAGE_UP = 33;
const PAGE_DOWN = 34;
const END = 35;
const HOME = 36;
const LEFT_ARROW = 37;
const UP_ARROW = 38;
const RIGHT_ARROW = 39;
const DOWN_ARROW = 40;
//const INSERT = 45;
const DELETE = 46;

export const keyCodesForMovement = new Set([
	PAGE_UP,
	PAGE_DOWN,
	END,
	HOME,
	LEFT_ARROW,
	UP_ARROW,
	RIGHT_ARROW,
	DOWN_ARROW,
]);

export const keyCodesToForwardOnChange = new Set([
	BACKSPACE,
	DELETE,
	...keyCodesForMovement,
]);
