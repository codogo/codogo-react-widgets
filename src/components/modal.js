import React from "react";
import { compose, } from "recompose";
import styled from "styled-components";

import Button from "./button";

const Overlay = styled.div`
	position: fixed;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	background: rgba(0, 0, 0, 0.5);
	z-index: 100;
	padding: 4em;
	cursor: pointer;
	display: block;
`;

const Box = styled.div`
	background: white;
	margin: 0 auto;
	width: 100%;
	max-width: 800px;
	cursor: auto;
	padding: 2.5em;
	display: block;

	& > ${ Button.Container } {
		flex-shrink: 0;
	}
`;

const Title = styled.div`
	font-size: 1.6em;
	flex-shrink: 0;
`;

const Content = styled.div`
	display: block;
	flex-grow: 1;
	flex-shrink: 1;
	margin: 1.5em 0;
	overflow-y: auto;
	overflow-x: hidden;
`;

const enhance = compose();

const stopPropagation = e => e.stopPropagation();

const noop = () => {};

const Modal = props =>
	props.visible ? (
		<Overlay onClick = { props.noDismiss ? noop : props.close }>
			<Box onClick = { stopPropagation }>
				<Title>{props.title || "<Modal title goes here>"}</Title>
				<Content>{props.children}</Content>
				<Button.Container alignRight>
					{props.options ? (
						props.options.map(({ label, fn, type, }, i) => (
							<Button
								type = { type || "CONFIRM" }
								onClick = { fn }
								text = { label }
								key = { i }
							/>
						))
					) : (
						<Button
							type = "CONFIRM"
							onClick = { props.close }
							text = "Close"
						/>
					)}
				</Button.Container>
			</Box>
		</Overlay>
	) : null;

export default enhance(Modal);

// EXAMPLE MODAL
// don't delete this unless you're gonna put this info somewhere else
//
// <Modal
// 	close = { someFunction }
// 	visible = { someBoolean }
// 	title = "someString"
// 	options = {
// 		[
// 			{
// 				label: "someString",
// 				fn: someFunction,
// 				type: "SOME_TYPE",
// 			},
// 			...
// 		]
// 	}
// >
// 	{ whateverContentYouWant }
// </Modal>
