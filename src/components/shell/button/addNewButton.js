import React from "react";
import * as R from "ramda";
import { withHandlers, compose, } from "recompose";
import gql from "graphql-tag";
import { withApollo, graphql, } from "react-apollo";

import { AddOrgButton, } from "../styles";

//------------------------------

const getUserID = gql`
	query getUserID {
		user {
			id
		}
	}
`;

const createNewOrg = gql`
	mutation createNewOrg {
		createOrg(name: "New Org") {
			id
		}
	}
`;

const addStuffToNewOrg = gql`
	mutation($userId: ID!, $orgId: ID!) {
		createMembership(type: ADMIN, userId: $userId, orgId: $orgId) {
			id
			org {
				id
				memberships {
					id
					user {
						id
					}
				}
			}
			user {
				id
				memberships {
					id
					type
					org {
						id
					}
				}
			}
		}
		createFolder(
			orgId: $orgId
			parentOrgId: $orgId
			name: "__ROOT_FOLDER__"
		) {
			id
			name
			org {
				id
				rootFolder {
					id
					name
				}
			}
		}
	}
`;

const enhance = compose(
	graphql(getUserID, {
		name: "getUserID",
	}),
	withApollo,
	withHandlers({
		addNewOrg: ({ client, getUserID, }) => () =>
			client
				.mutate({
					mutation: createNewOrg,
				})
				.then(R.path([ "data", "createOrg", "id", ]))
				.then(orgId =>
					client.mutate({
						mutation: addStuffToNewOrg,
						variables: {
							userId: R.path([ "user", "id", ], getUserID),
							orgId,
						},
					}),
				),
	}),
);

export default enhance(
	props =>
		R.path([ "getUserID", "user", "id", ], props) ? (
			<AddOrgButton onClick = { props.addNewOrg } title = "Add new org">
				Add new org
			</AddOrgButton>
		) : null,
);
