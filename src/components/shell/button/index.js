import React from "react";
import { withRouter, } from "react-router";
import { compose, withProps, } from "recompose";

import AddNewButton from "./addNewButton";
import OrgButton from "./orgButton";
//import PlaceholderButton from "./placeholderButton";
import UserButton from "./userButton";

const enhance = compose(
	withRouter,
	withProps(ownProps => ({
		active:
			ownProps.id &&
			ownProps.id === R.path([ "match", "params", "orgID", ])(ownProps),
	})),
);

export default enhance(
	props =>
		props.isAddNew ? (
			<AddNewButton { ...props } />
		) : props.isOrg ? (
			<OrgButton { ...props } />
		) : props.isUser ? (
			<UserButton { ...props } />
		) : null,
);
