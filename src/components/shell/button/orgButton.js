import React from "react";
import * as R from "ramda";
import { Link, } from "react-router-dom";
import { withTheme, } from "styled-components";
import gql from "graphql-tag";
import { graphql, } from "react-apollo";

import CloudinaryImage from "../../cloudinaryImage";
import { SideBarButtonImage, } from "../styles";

//------------------------------

const getOrg = gql`
	query GetOrgForSidebar($id: ID!) {
		Org(id: $id) {
			id
			name
			image
		}
	}
`;

const getLinkTo = props =>
	"/app/" + R.pathOr("", [ "getOrg", "Org", "id", ], props);

const getImageID = R.path([ "getOrg", "Org", "image", "cloudinaryID", ]);

//------------------------------

export default R.compose(
	graphql(getOrg, {
		name: "getOrg",
		skip: ownProps => ownProps.placeholder || ownProps.addNew,
		options: ownProps => ({
			variables: {
				id: ownProps.id,
			},
		}),
	}),
	withTheme,
)(props => (
	<Link to = { getLinkTo(props) }>
		<SideBarButtonImage active = { props.active }>
			<CloudinaryImage
				isButton
				height = { props.theme.sizes.sidebarButtonSize }
				width = { props.theme.sizes.sidebarButtonSize }
				transformation = { CloudinaryImage.commonTransforms.smartSquare }
				cloudinaryID = { getImageID(props) }
			/>
		</SideBarButtonImage>
	</Link>
));
