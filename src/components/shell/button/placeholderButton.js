import styled from "styled-components";

//------------------------------

const OrgImage = styled.div`
	background-color: white;
	border-radius: 4px;
	height: 40px;
	margin: 4px;
	padding: 4px;
	width: 40px;
`;

//------------------------------

export default () => <OrgImage>loading...</OrgImage>;
