import React from "react";
import * as R from "ramda";
import { Link, } from "react-router-dom";
import { withTheme, } from "styled-components";
import gql from "graphql-tag";
import { graphql, } from "react-apollo";

import CloudinaryImage from "../../cloudinaryImage";
import { SideBarButtonImage, } from "../styles";

//------------------------------

const getUser = gql`
	query getUserForSidebar {
		user {
			id
			username
			image
			personal {
				id
			}
		}
	}
`;

const getLinkTo = props =>
	"/app/" + R.pathOr("", [ "getUser", "user", "personal", "id", ], props);

const getImageID = R.path([ "getUser", "user", "image", "cloudinaryID", ]);

//------------------------------

export default R.compose(
	graphql(getUser, {
		name: "getUser",
		skip: ownProps => ownProps.placeholder || ownProps.addNew,
	}),
	withTheme,
)(props => (
	<Link to = { getLinkTo(props) }>
		<SideBarButtonImage active = { props.active }>
			<CloudinaryImage
				isButton
				height = { props.theme.sizes.sidebarButtonSize }
				width = { props.theme.sizes.sidebarButtonSize }
				transformation = { CloudinaryImage.commonTransforms.smartSquare }
				cloudinaryID = { getImageID(props) }
			/>
		</SideBarButtonImage>
	</Link>
));
