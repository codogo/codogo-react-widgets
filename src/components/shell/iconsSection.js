import React from "react";
import { compose, withStateHandlers, } from "recompose";
import styled from "styled-components";
import { Link, } from "react-router-dom";

import BaseIcon from "../icon";
import ReportModal from "./reportModal";

import { SidebarIconsSection, } from "./styles";
import { getUserContext, } from "../../enhancers/withUserContext";

// --------------------------------------------------

const Icon = styled(BaseIcon)`
	opacity: 0.67;
	transition: 0.1s linear opacity;
	cursor: pointer;
	width: ${ ({ theme, }) => theme.sizes.sidebarIconSize }px;
	height: ${ ({ theme, }) => theme.sizes.sidebarIconSize }px;
	margin-top: ${ ({ theme, }) => theme.sizes.sidebarIconSize * 0.2 }px;

	&:hover {
		opacity: 1;
	}
`;

Icon.defaultProps = {
	stroke: "#fff",
};

const enhance = compose(
	getUserContext,
	withStateHandlers(
		{ reportModalVisible: false, },
		{
			closeReportModal: () => () => ({
				reportModalVisible: false,
			}),

			showReportModal: () => () => ({
				reportModalVisible: true,
			}),
		},
	),
);

const IconsSection = props => (
	<SidebarIconsSection>
		<Icon iconKey = "help-circle" onClick = { props.showReportModal } />

		<Link to = "/app">
			<Icon iconKey = "home" />
		</Link>

		<a href = "https://account.codogo.io/app">
			<Icon iconKey = "user" />
		</a>

		{props.reportModalVisible && (
			<ReportModal
				close = { props.closeReportModal }
				visible = { props.reportModalVisible }
			/>
		)}
	</SidebarIconsSection>
);

export default enhance(IconsSection);
