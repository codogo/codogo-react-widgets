import React from "react";
import { ApolloProvider, } from "react-apollo";
import { BrowserRouter, Route, Switch, Redirect, } from "react-router-dom";
import { ThemeProvider, injectGlobal, } from "styled-components";

import { withUserContext, } from "../../enhancers/withUserContext";
import { client, } from "../../apolloClient";
import theme from "../../theme";
import getJWT from "../../getJWT";

import Sidebar from "./sidebar";
import { Container, } from "./styles";

//------------------------------

const defaultTheme = theme;
const defaultClient = client;
const defaultRouter = BrowserRouter;

//------------------------------

const EnhancedContainer = withUserContext(Container);

const renderContained = props => () => (
	<EnhancedContainer>
		<Sidebar showAccountLink = { props.showAccountLink } />
		{props.children}
	</EnhancedContainer>
);

/* eslint no-unused-expressions: 0 */
injectGlobal`
	@import url('https://fonts.googleapis.com/css?family=Rubik:400,400i');

	html, body, #root {
		width: 100% ;
		height: 100% ;
		min-height: 100% ;
		margin: 0;
	}

	*, *:before, *:after {
		box-sizing: border-box;
	}

	body {
		font-family: Rubik, Helvetica Neue,Segoe UI,Helvetica,Arial,sans-serif;
		color: ${ theme.colors.black };
	}

	div {
		display: flex;
		flex-direction: column;
	}

	h1, h2, h3, h4, h5, h6, p {
		margin: 0;
	}

	a, a:hover, a:visited, a:active, a:link {
		color: ${ theme.colors.black };
		color: currentColor;
		text-decoration: none;
	}
`;

const AppRedirect = () => <Redirect to = "/app" />;

class Shell extends React.Component {
	render() {
		const Router = this.props.router;

		return (
			<ThemeProvider theme = { this.props.theme }>
				<ThemeProvider theme = { this.props.themeMutator }>
					<ApolloProvider client = { this.props.client }>
						<Router>
							<Switch>
								<Route
									path = "/app/:orgID?"
									render = { renderContained(this.props) }
								/>
								<Route component = { AppRedirect } />
							</Switch>
						</Router>
					</ApolloProvider>
				</ThemeProvider>
			</ThemeProvider>
		);
	}
}

Shell.defaultRouter = defaultRouter;
Shell.defaultClient = defaultClient;
Shell.defaultTheme = defaultTheme;
Shell.getJWT = getJWT;

Shell.defaultProps = {
	router: defaultRouter,
	client: defaultClient,
	theme: defaultTheme,
	themeMutator: x => x,
};

export default Shell;
