import React from "react";
import styled from "styled-components";

import Modal from "../modal";
import Button from "../button";

// --------------------------------------------------

const Wrapper = styled.div`
	overflow: hidden;
`;

export default props => (
	<Modal { ...props } title = "Get In Touch">
		<Wrapper>
			<Button.Container>
				<Button
					href = "https://feedback.userreport.com/1f1b35d7-980f-41c2-a056-bbb47daae2b8/#submit/bug"
					target = "_blank"
					rel = "noopener noreferrer"
					text = "Submit Bug Report"
					iconKey = "alert-circle"
				/>
				<Button
					href = "https://feedback.userreport.com/1f1b35d7-980f-41c2-a056-bbb47daae2b8/#submit/idea"
					target = "_blank"
					rel = "noopener noreferrer"
					text = "Submit Feature Request"
					iconKey = "plus-circle"
				/>
			</Button.Container>

			<br />

			<Button.Container>
				<Button
					href = "https://twitter.com/CodogoIO"
					target = "_blank"
					rel = "noopener noreferrer"
					colorOverride = "#1DA1F2"
					text = "Twitter"
					iconKey = "twitter"
				/>
				<Button
					href = "https://codogo-slack.now.sh"
					target = "_blank"
					rel = "noopener noreferrer"
					colorOverride = "#e01563"
					text = "Slack"
					iconKey = "slack"
				/>
				<Button
					href = "mailto:codes@codogo.io"
					target = "_blank"
					rel = "noopener noreferrer"
					colorOverride = "#444"
					text = "Email"
					iconKey = "mail"
				/>
			</Button.Container>
		</Wrapper>
	</Modal>
);
