import React from "react";
import * as R from "ramda";
import { compose, withProps, } from "recompose";
import gql from "graphql-tag";
import { graphql, } from "react-apollo";
import { withRouter, } from "react-router";

import Button from "./button";
import IconsSection from "./iconsSection";
import { Sidebar, Arrow, SidebarOrgsSection, } from "./styles";

// --------------------------------------------------

const userQuery = gql`
	query SidebarUserQuery {
		user {
			id
			username
			personal {
				id
				name
			}
			memberships {
				id
				type
				org {
					id
				}
			}
		}
	}
`;

const enhance = compose(
	graphql(userQuery),
	withRouter,
	withProps(({ data, }) => ({
		orgIDs: R.pipe(
			R.pathOr([], [ "user", "memberships", ]),
			R.map(R.path([ "org", "id", ])),
		)(data),

		personalOrgID: R.path([ "user", "personal", "id", ], data),
	})),
	withProps(props => ({
		allIDs: [
			props.personalOrgID,
			...props.orgIDs.filter(orgID => orgID !== props.personalOrgID),
		],
	})),
	withProps(props => ({
		activeOrgIndex: props.allIDs.findIndex(
			id => id && id === R.path([ "match", "params", "orgID", ])(props),
		),
	})),
);

export default enhance(props => {
	return props.data.user ? (
		<Sidebar>
			<SidebarOrgsSection>
				<Button isUser id = { props.personalOrgID } />

				{props.orgIDs
					.filter(orgID => orgID !== props.personalOrgID)
					.map(orgID => <Button isOrg key = { orgID } id = { orgID } />)}

				<Button isAddNew />

				{props.activeOrgIndex > -1 ? (
					<Arrow index = { props.activeOrgIndex } />
				) : null}
			</SidebarOrgsSection>

			<IconsSection { ...props } />
		</Sidebar>
	) : (
		<Sidebar />
	);
});
