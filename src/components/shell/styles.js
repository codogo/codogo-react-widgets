import styled from "styled-components";
import * as R from "ramda";
import AddIcon from "feather-icons/dist/icons/plus.svg";

// --------------------------------------------------

const gutter = 8;
const arrow = 12;

// --------------------------------------------------

export const Container = styled.div`
	padding-left: ${ ({ theme, }) =>
		theme.sizes.sidebarButtonSize + 2 * gutter }px;
	width: 100%;
	height: 100%;
	min-height: 100%;
`;

// --------------------------------------------------

export const Sidebar = styled.div`
	align-items: center;
	background-color: ${ R.path([ "theme", "colors", "primary", ]) };
	box-shadow: 0px 0px 8px rgba(0, 0, 0, 0);
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	overflow: hidden;
	z-index: 1;
	position: fixed;
	top: 0;
	bottom: 0;
	left: 0;
	width: ${ ({ theme, }) => theme.sizes.sidebarButtonSize + 2 * gutter }px;
`;

export const SidebarOrgsSection = styled.div`
	padding: ${ gutter / 2 }px 0;

	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: center;
`;

export const SidebarIconsSection = styled.div`
	padding: ${ ({ theme, }) =>
		theme.sizes.sidebarButtonSize - theme.sizes.sidebarIconSize + gutter }px;
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: center;
	width: 100%;
`;

// --------------------------------------------------

export const SideBarButtonImage = styled.div`
	border-radius: ${ gutter }px;
	margin: ${ gutter / 2 }px ${ gutter }px;
	overflow: hidden;
	position: relative;

	&:after {
		position: absolute;
		top: 50%;
		right: 0;
		margin-top: -${ arrow }px;

		border: ${ arrow }px solid transparent;
		border-left-width: 0;
		border-right-color: white;
		border-right-width: ${ arrow * 1.4 }px;
	}

	&:hover,
	&:active {
		opacity: ${ R.path([ "theme", "colors", "translucent", ]) };
	}
`;

// --------------------------------------------------

export const Arrow = styled.div`
	position: absolute;
	top: ${ props =>
		gutter +
		props.theme.sizes.sidebarButtonSize / 2 +
		props.index * (gutter + props.theme.sizes.sidebarButtonSize) }px;
	right: 0;
	margin-top: -${ arrow }px;

	border: ${ arrow }px solid transparent;
	border-left-width: 0;
	border-right-color: white;
	border-right-width: ${ arrow * 1.4 }px;

	transition: 0.25s ease-out top;
`;

// --------------------------------------------------

//Add Org Button
const AOBWrapper = styled.div`
	width: ${ R.path([ "theme", "sizes", "sidebarButtonSize", ]) }px;
	height: ${ R.path([ "theme", "sizes", "sidebarButtonSize", ]) }px;
	border: 2px dashed white;
	border-radius: ${ gutter }px;
	margin: ${ gutter / 2 }px;
	display: flex;
	justify-content: center;
	align-items: center;
	cursor: pointer;
	opacity: 0.5;
	transition: 0.15s ease-out all;

	&:hover {
		opacity: 1;
		border-style: solid;
		background: rgba(255, 255, 255, 0.3);
	}
`;

const iconSize = "70%";

const AOBIcon = () => (
	<AddIcon
		width = { iconSize }
		height = { iconSize }
		stroke = "white"
		strokeWidth = "2"
	/>
);

export const AddOrgButton = props => (
	<AOBWrapper { ...props }>
		<AOBIcon />
	</AOBWrapper>
);
