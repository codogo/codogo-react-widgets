import React from "react";
import * as R from "ramda";
import styled from "styled-components";
import CloudinaryImage from "./cloudinaryImage";

// --------------------------------------------------

const Table = styled.div`
	display: flex;
	flex-direction: column;
	align-items: stretch;
`;

const Row = styled.div`
	display: flex;
	flex-direction: row;
	flex-shrink: 0;
	${ props => (props.onClick ? "cursor: pointer;" : "") };
`;

const ContentRow = styled(Row)`
	border-bottom: 1px solid ${ R.path([ "theme", "colors", "borders", ]) };
	&:hover {
		background: ${ R.path([ "theme", "colors", "lightergray", ]) };
	}
`;

const HeaderRow = styled(Row)`
	border-bottom: 1px solid ${ R.path([ "theme", "colors", "black", ]) };
	font-weight: bold;
`;

const Cell = styled.div`
	height: 3em;
	flex-direction: row;
	justify-content: flex-start;
	flex: 1;
	align-items: center;
	padding: 0 1em;

	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	min-width: 0;
`;

const RowImageWrapper = styled.div`
	margin-right: 0.5em;
`;

const RowImage = image => (
	<RowImageWrapper>
		<CloudinaryImage { ...image } height = { 32 } width = { 32 } />
	</RowImageWrapper>
);

const RowCell = props => (
	<ContentRow { ...props }>
		<Cell>{props.children}</Cell>
	</ContentRow>
);

Table._Row = Row;
Table.Row = ContentRow;
Table.HeaderRow = HeaderRow;
Table.Cell = Cell;
Table.Avatar = RowImage;
Table.RowCell = RowCell;

export default Table;
