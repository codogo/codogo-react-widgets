import React from "react";
import * as R from "ramda";
import styled from "styled-components";

import { getPaddingVertical, getPaddingHorizontal, } from "../getters";

// --------------------------------------------------

const getHalfPaddingVertical = R.pipe(getPaddingVertical, R.multiply(0.5));
const getHalfPaddingHorizontal = R.pipe(getPaddingHorizontal, R.multiply(0.5));

const getGray = R.path([ "theme", "colors", "gray", ]);
const getDarkGray = R.path([ "theme", "colors", "darkgray", ]);
const getMidGray = R.path([ "theme", "colors", "midgray", ]);

// --------------------------------------------------

const TableBody = styled.table`
	border-collapse: seperate;
	border: 1px solid ${ R.path([ "theme", "colors", "gray", ]) };
	background: ${ R.path([ "theme", "colors", "lightgray", ]) };
	border-radius: 3px;
	border-spacing: 0;
`;

const TableRow = styled.tr`
	opacity: ${ props => (props.visible === false ? 0.5 : 1) };
	font-weight: ${ props => (props.bold ? "bold" : "normal") };

	&:first-child {
		th,
		td {
			&:first-child {
				border-top-left-radius: 3px;
			}

			&:last-child {
				border-top-right-radius: 3px;
			}
		}
	}

	&:last-child {
		th,
		td {
			&:first-child {
				border-bottom-left-radius: 3px;
			}

			&:last-child {
				border-bottom-right-radius: 3px;
			}
		}
	}

	&:nth-child(2n + 1) {
		background: ${ getMidGray };
	}
`;

const TableCell = styled.td`
	padding: ${ getHalfPaddingVertical }em ${ getHalfPaddingHorizontal }em;

	&:hover {
		background: ${ getGray };
	}
`;

const TableHeaderCell = TableCell.withComponent("th").extend`
	font-weight: bold;
	text-align: left;
	background: ${ getGray };

	&:hover {
		background: ${ getDarkGray };
	}	
`;

// --------------------------------------------------

class Table extends React.Component {
	render() {
		return (
			<TableBody>
				<tbody>{this.props.children}</tbody>
			</TableBody>
		);
	}
}

// --------------------------------------------------

Table.HeaderRow = props => (
	<TableRow bold { ...props }>
		{React.Children.map(props.children, child => {
			return React.cloneElement(child, {
				inHeader: true,
			});
		})}
	</TableRow>
);

Table.Row = props => <TableRow { ...props } />;

Table.Cell = props =>
	props.inHeader ? <TableHeaderCell { ...props } /> : <TableCell { ...props } />;

// --------------------------------------------------

export default Table;
