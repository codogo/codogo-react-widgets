export const scapholdAuthTokenStorageKey = "idToken_auth0";
export const accountURL = "https://account.codogo.io";

export const graphcoolURL = "https://api.graph.cool/simple/v1/codogo";
export const graphcoolSubURL = "wss://subscriptions.graph.cool/v1/codogo";
