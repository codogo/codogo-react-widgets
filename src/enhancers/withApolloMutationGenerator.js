import React from "react";
import { graphql, withApollo, compose, } from "react-apollo";

const withApolloMutationGenerator = (query, options) => {
	const { path, name = "data", } = options || {};

	const queryPath = typeof path === "string" ? [ path, ] : path;

	return Component =>
		compose(graphql(query), withApollo)(
			class GeneratedMutations extends React.Component {
				render() {
					const queryResult = this.props[name];

					let queryDecender = queryResult;
					queryPath.forEach(prop => {
						queryDecender = queryDecender[prop];
					});

					this.id = queryDecender.id;

					console.log({ queryDecender, queryPath, name, });

					return <Component { ...queryDecender } />;
				}
			},
		);
};

export default withApolloMutationGenerator;
