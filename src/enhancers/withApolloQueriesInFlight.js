import React from "react";
import * as R from "ramda";

export default Comp =>
	class WithApolloQueriesInFlight extends React.Component {
		constructor(props) {
			super(props);

			this.state = {
				queryStatuses: {},
				mutationsLoading: {
					false: 0,
				},
			};
		}

		render() {
			console.warn("WithApolloQueriesInFlight doesn't work anymore");

			return <Comp { ...R.omit([ "client", ])(this.props) } { ...this.state } />;
		}
	};
