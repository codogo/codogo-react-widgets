import React from "react";
import { compose, withState, withHandlers, withPropsOnChange, } from "recompose";

import ContextMenu from "../components/contextMenu";

// --------------------------------------------------

const defaultOpts = [
	{
		label: "noop",
		fn: () => {},
	},
];

const withContextMenu = compose(
	withState("contextMeta", "setContextMeta", {
		x: 0,
		y: 0,
		isOpen: false,
	}),
	withHandlers({
		onContextMenu: ({ setContextMeta, }) => e => {
			const { clientX: x, clientY: y, } = e;
			setContextMeta(() => ({
				x,
				y,
				isOpen: true,
			}));
			e.stopPropagation();
			e.preventDefault();
		},
		closeContextMenu: ({ setContextMeta, }) => () =>
			setContextMeta(R.assoc("isOpen", false)),
	}),
	// options are set through props rather than as an arg of withContextMenu
	// to let you do your own memoizing using withPropsOnChange, instead of calling
	// a function on every single render (eg, if it were: "options = { createOptions(props) }")
	withPropsOnChange(
		[ "children", "contextMeta", "contextMenuOptions", "cmOptions", ],
		props => ({
			children: props.contextMeta.isOpen
				? React.Children.toArray(props.children).concat(
					<ContextMenu
						x = { props.contextMeta.x }
						y = { props.contextMeta.y }
						options = {
							props.contextMenuOptions ||
								props.cmOptions ||
								defaultOpts
						}
						close = { props.closeContextMenu }
						key = "context-menu"
					/>,
				)
				: props.children,
			ContextMenu: () =>
				props.contextMeta.isOpen ? (
					<ContextMenu
						x = { props.contextMeta.x }
						y = { props.contextMeta.y }
						options = {
							props.contextMenuOptions ||
							props.cmOptions ||
							defaultOpts
						}
						close = { props.closeContextMenu }
						key = "context-menu"
					/>
				) : null,
			wtf: "why?",
		}),
	),
);

export default withContextMenu;
