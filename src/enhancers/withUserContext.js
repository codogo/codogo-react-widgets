import { compose, withContext, getContext, } from "recompose";
import PropTypes from "prop-types";
import gql from "graphql-tag";
import { graphql, } from "react-apollo";

// --------------------------------------------------

const userQuery = gql`
	query($filter: UserFilter) {
		user {
			id
			username
			personal {
				id
				name
			}
			memberships(filter: { org: { personalOf: $filter } }) {
				id
				type
				org {
					id
				}
			}
		}
	}
`;

export const withUserContext = compose(
	graphql(userQuery, {
		options: {
			fetchPolicy: "network-only",
			variables: {
				filter: null,
			},
		},
	}),
	withContext(
		{
			user: PropTypes.object,
			userLoading: PropTypes.bool,
			userRefetch: PropTypes.func,
			userObject: PropTypes.object,
		},
		props => ({
			user: props.data.user,
			userLoading: props.data.loading,
			userRefetch: props.data.refetch,
			userObject: props.data.user || {},
		}),
	),
);

export const getUserContext = getContext({
	user: PropTypes.object,
	userLoading: PropTypes.bool,
	userRefetch: PropTypes.func,
	userObject: PropTypes.object,
});
