import jwtDecode from "jwt-decode";
import Channel from "js-channel";
import moment from "moment";

import { accountURL, scapholdAuthTokenStorageKey, } from "./consts";

const tokenIsValid = jwt => {
	try {
		jwtDecode(jwt);
		return true;
	} catch (e) {
		return false;
	}
};

const tokenInDate = jwt => {
	try {
		return moment.unix(jwtDecode(jwt).exp).isAfter(moment());
	} catch (e) {
		return false;
	}
};

const shouldTryToVendJWT = () => window.location.origin === accountURL;

const callbackQueue = [];

const redirectToGetToken = () => {
	window.location = `${ accountURL }/app/login?come_from=${ window.location }`;
};

const retrivedJWT = jwt => {
	if (shouldTryToVendJWT()) {
		return false;
	}

	if (!jwt || !tokenInDate(jwt)) {
		return redirectToGetToken();
	} else {
		localStorage.setItem(scapholdAuthTokenStorageKey, jwt);
	}

	callbackQueue.forEach(cb => cb(jwt));
};

if (document.getElementById("codogo-account-iframe")) {
	Channel.build({
		window: document.getElementById("codogo-account-iframe").contentWindow,
		origin: "*",
		scope: "jwtVending",
		onReady: chan => {
			chan.call({
				method: "getJWT",
				success: retrivedJWT,
				error: redirectToGetToken,
			});
		},
	});
}

const getJWTFromIFrame = () => {
	if (shouldTryToVendJWT()) {
		return false;
	}

	return new Promise(done => callbackQueue.push(done));
};

export default () =>
	Promise.resolve(localStorage.getItem(scapholdAuthTokenStorageKey)).then(
		jwt =>
			jwt && tokenIsValid(jwt) && tokenInDate(jwt)
				? jwt
				: getJWTFromIFrame(),
	);
