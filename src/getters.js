import { css, } from "styled-components";

export const getSizeValue = R.curry((x, y) => R.path([ "theme", "sizes", x, y, ]));

export const getPadding = getSizeValue("padding");
export const getPaddingHorizontal = getPadding("horizontal");
export const getPaddingVertical = getPadding("vertical");

export const getMargin = getSizeValue("margin");
export const getMarginHorizontal = getMargin("horizontal");
export const getMarginLeft = getMargin("left");
export const getMarginRight = getMargin("right");
export const getMarginVertical = getMargin("vertical");
export const getMarginFourEm = css`
	${ getMarginVertical }em ${ getMarginRight }em ${ getMarginVertical }em ${ getMarginLeft }em;
`;

export const getPadding2 = theme =>
	`${ getPaddingVertical(theme) }em ${ getPaddingHorizontal(theme) }em `;

export const getMargin2 = theme =>
	`${ getMarginVertical(theme) }em ${ getMarginHorizontal(theme) }em `;

export const getArrowWidth = R.path([ "theme", "sizes", "arrowWidth", ]);
export const getBorderRadius = R.path([ "theme", "sizes", "borderRadius", ]);

export const getColor = x => R.path([ "theme", "colors", x, ]);
