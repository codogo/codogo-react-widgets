// -------------------------------------

const colors = {
	accounts: "#ff5559",
	write: "#f60",

	//default value, must be overwritten
	primary: "#aaa",
	secondary: "#aaa",

	white: "#fff",
	lightgray: "#eee",
	lightergray: "#f8f8f8",
	midgray: "#ddd",
	gray: "#aaa",
	darkgray: "#666",
	black: "#333",

	red: "#f92672",
	orange: "#fd971f",
	yellow: "#e6db74",
	green: "#a6e22e",
	blue: "#66d9ef",
	purple: "#ae81ff",

	text: "#333",
	borders: "#eee",
	background: "#fff",

	lightmodetext: "blue",
	lightmodebg: "#fff",
	darkmodetext: "#ccc",
	darkmodebg: "#141E26",

	transparent: 0.33,
	translucent: 0.7,
	opaque: 0.85,
};

// -------------------------------------

const units = {
	tiny: 0.25,
	small: 0.5,
	medium: 0.75,
	large: 1,
};

const sizes = {
	...units,
	padding: {
		horizontal: units.large,
		vertical: units.small,
	},
	margin: {
		horizontal: 0,
		left: 0,
		right: 0,
		vertical: units.small,
	},
	borderRadius: "3px",
	arrowWidth: 0.6,
	sidebarButtonSize: 40,
	sidebarIconSize: 30,
	button: {
		gutter: units.small,
	},
};

// -------------------------------------

const shadow = (height, over = 0) => {
	const h = height - over;

	return {
		"0": "",
		"1":
			"box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);",
		"2":
			"box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);",
		"3":
			"box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);",
		"4":
			"box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);",
		"5":
			"box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);",
		"-1":
			"box-shadow: inset 0 -1px 3px rgba(0,0,0,0.12), inset 0 -1px 2px rgba(0,0,0,0.24);",
	}[h];
};

const shadowProxy = new Proxy(
	{},
	{
		get(target, key) {
			return {
				"0": "",
				"1":
					//"box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);",
					"box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.08);",
				"2":
					//"box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);",
					"box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.08);",
				"3":
					//"box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);",
					"box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.08);",
				"4":
					//"box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);",
					"box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.08);",
				"5":
					//"box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);",
					"box-shadow: 0 16px 32px 0 rgba(0, 0, 0, 0.08);",
				"-1":
					//"box-shadow: inset 0 -1px 3px rgba(0,0,0,0.12), inset 0 -1px 2px rgba(0,0,0,0.24);",
					"box-shadow: inset 0 -1px 2px rgba(0,0,0,0.08), inset 0 -1px 2px rgba(0,0,0,0.18);",
			}[key];
		},
	},
);

// -------------------------------------

export default {
	colors: colors,
	sizes: sizes,
	functions: {
		shadowProxy, //allows you to pass arguments to functions using only R.path
		shadow: shadow,
	},
};
